const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');

const User = mongoose.model('User');

//Register Api With Save Functionality
module.exports.register = (req, res, next) => {
    var user = new User();
    user.name = req.body.name;
    user.email = req.body.email;
    user.password = req.body.password
    user.mobile = req.body.mobile;
    user.gender = req.body.gender;
    user.save((err, doc) => {
        if (!err){
            let success = {
                success:{
                    message:"User Added."
                }
            }
            console.log(doc);
        console.log(success);
            res.send(success);
        }
        else {
            if (err.code == 11000)
                res.status(422).send(['Duplicate user name found.']);
            else{
                let error = {
                    error:{
                        message:"Failed to add user."
                    }
                }
                return next(err);
            }
                
        }

    });
}

//Authentication Login user Credential
module.exports.authenticate = (req, res, next) => {
    // call for passport authentication
    passport.authenticate('local', (err, user, info) => {
        // error from passport middleware
        if (err) return res.status(400).json(err);
        // registered user
        else if (user) return res.status(200).json({ "token": user.generateJwt() });
        // unknown user or wrong password
        else return res.status(404).json(info);
    })(req, res);
}

// For Get User profile data
module.exports.userProfile = (req, res, next) => {
    User.findOne({ _id: req._id },
        (err, user) => {
            if (!user)
                return res.status(404).json({ status: false, message: 'User record not found.' });
            else
                return res.status(200).json({ status: true, user: _.pick(user, ['email', 'password']) });
        }
    );
}
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the HttpRestClientProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpRestClientProvider {
  constructor(public http: HttpClient) { }
  
  postData(postUrl, data,headersConfig): Observable<Object> {
    return this.http.post(postUrl, data,headersConfig);
  }

  getData(url): Observable<Object> {
    return this.http.get(url);
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AlertController,Events } from 'ionic-angular';
import { Network } from '@ionic-native/network';
/*
  Generated class for the NetworkProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
export enum ConnectionStatusEnum {
  Online,
  Offline
}
@Injectable()

export class NetworkProvider {
  previousStatus;
  constructor(public http: HttpClient, public alertCtrl: AlertController, private network: Network,public eventCtrl: Events) {

    this.previousStatus = ConnectionStatusEnum.Online;
    
  }
  

  public initializeNetworkEvents(): void {
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
     
    });
    disconnectSubscription.unsubscribe();
    let connectSubscription = this.network.onConnect().subscribe(() => {
     
      // We just got a connection but we need to wait briefly
       // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        if (this.network.type === 'wifi') {
         
        }
      }, 3000);
    });
    
    // stop connect watch
    connectSubscription.unsubscribe();
}

}

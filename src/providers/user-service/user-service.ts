import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpRestClientProvider }  from './../http-rest-client/http-rest-client';
/*
  Generated class for the UserServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserServiceProvider {

  constructor(public http: HttpClient, public _httpRequestProvider:HttpRestClientProvider) {
  }
  
  
  getuserData(){
  }
  postfeedbackData(dataFinal,url){
    let headers = {
      'Content-Type': 'application/json'
  }
    let headersConfig = new HttpHeaders(headers);
    let response: any = this._httpRequestProvider.postData(url,dataFinal,{headers:headersConfig});
    
    return response;
  }
}

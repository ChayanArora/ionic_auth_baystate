import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {IonicStorageModule} from '@ionic/storage'
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
// import {FeedbackPage}  from '../pages/feedback/feedback';
import {HeaderComponent} from '../components/header/header';
import { HttpRestClientProvider } from '../providers/http-rest-client/http-rest-client'; 
import { HttpClientModule } from '@angular/common/http'; 
import { UserServiceProvider } from '../providers/user-service/user-service';
import { NetworkProvider } from '../providers/network/network';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { LoginPage } from '../pages/login/login';
import { NewsfeedPage } from '../pages/newsfeed/newsfeed';
import { PopoverPage } from '../pages/popover/popover';
import { ViewUserPage } from '../pages/view-user/view-user';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    HeaderComponent,
    NewsfeedPage,
    PopoverPage,
    ViewUserPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule, 
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    NewsfeedPage,
    PopoverPage,
    ViewUserPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpRestClientProvider,
    UserServiceProvider,
    NetworkProvider,
    InAppBrowser
  ]
})
export class AppModule {}

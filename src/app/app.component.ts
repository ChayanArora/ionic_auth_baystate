import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { NetworkProvider } from '../providers/network/network';
import { Keyboard } from '@ionic-native/keyboard';




// import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
@Component({
  templateUrl: 'app.html',
  providers: [NetworkProvider,Keyboard]
})
export class MyApp {
  // rootPage: any = HomePage;
  rootPage: any = LoginPage;
  constructor(private keyboard: Keyboard, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public events: Events, public network: Network, public networkProvider: NetworkProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#3f3d77');
      splashScreen.hide();
      this.keyboard.hideFormAccessoryBar(false)
      // Keyboard.hideKeyboardAccessoryBar(false);
    });
  }

  initializeApp() {

    this.platform.ready().then(() => {

      // this.networkProvider.initializeNetworkEvents();

      // Offline event
      this.events.subscribe('network:offline', () => {
        alert('network:offline ==> ' + this.network.type);
      });

      // Online event
      this.events.subscribe('network:online', () => {
        alert('network:online ==> ' + this.network.type);
      });

    });
  }


}


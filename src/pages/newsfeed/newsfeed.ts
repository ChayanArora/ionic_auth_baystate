import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import{Storage} from'@ionic/storage'

/**
 * Generated class for the NewsfeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-newsfeed',
  templateUrl: 'newsfeed.html',
})

export class NewsfeedPage {
  headTitle: string = "NewsFeed";
  feedData = this.navParams.get('feedData');

  constructor(public navCtrl: NavController,
    public navParams: NavParams,public viewCtrl: ViewController,
    public storage:Storage,
    public inApp: InAppBrowser) {
  }
  ionViewWillEnter() {
    this.viewCtrl.showBackButton(true);
}
  ionViewDidLoad() {
    
  };
  ionViewDidEnter() {
    this.storage.set("page","News")
  }
//InApp Browser to show news feeds
  showMore(item){
    this.inApp.create(item.url);
  }
  
}

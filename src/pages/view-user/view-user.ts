import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import {Storage} from '@ionic/storage';
import { HomePage } from '../home/home';
/**
 * Generated class for the ViewUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-user',
  templateUrl: 'view-user.html',
})
export class ViewUserPage {
dataArray=[];
headTitle: string = "View User";
  constructor(public stor :Storage,public events:Events,public navCtrl: NavController, public navParams: NavParams,public storage:Storage) {
  }
  
  ionViewDidLoad() {
    this.stor.set("page","View")
  }
   // Edit Single row data
   editData(i) {
this.navCtrl.push(HomePage,{
  data: this.dataArray,
  index:i
});
    // this.editValue = this.dataArray[i];
    // this.phoneField = this.editValue.mobile;
    // this.nameField = this.editValue.name;
    // this.gender = this.editValue.gender;
    // this.index = i;
  }
  ionViewDidEnter() {
    this.stor.get("user").then(val=>{
      this.dataArray = val;
    })
    this.stor.set("page","View")
}
}

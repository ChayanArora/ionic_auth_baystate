import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, ViewController } from 'ionic-angular';
import { HttpRestClientProvider } from './../../providers/http-rest-client/http-rest-client';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { Network } from '@ionic-native/network';
import 'rxjs/add/observable/fromEvent'
import { constant } from '../../constant';
import { Storage } from '@ionic/storage';


// import {Observable} from 'rxjs/Observable';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [HttpRestClientProvider, UserServiceProvider]
})

export class HomePage {
  editValue: any;
  userData: any
  phoneField: string;
  nameField: string;
  apiClitentID: string;
  category: any;
  titleHeader: string = "Home";
  dataArray = [];
  index: number;
  editIndex:number;
  gender: string = "Male";
  feedData: any;
  Username:string;
  constructor(private toast: ToastController,public viewCtrl: ViewController,
    private network: Network,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public _httpRequestProvider: HttpRestClientProvider,
    public userProv: UserServiceProvider,
    public storage: Storage) {


  }
  ionViewDidLoad() {
    
    if (!this.navParams.get('data')) {
      console.log("not edit")
      !this.storage.get('user').then(val => {
      if(val !=null)  {
        this.dataArray = val;
      }
        console.log(this.dataArray)
      })
    }else{
      this.dataArray = this.navParams.get('data')
      this.editIndex = this.navParams.get('index')
      this.editData(this.editIndex)
      console.log("edit value")
    }
  }
  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
}
  //For Network Checking
  ionViewDidEnter() {
    this.storage.set("page","Home")
    console.log("sdvjhsdfj")
    this.network.onConnect().subscribe(data => {
      
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

    this.network.onDisconnect().subscribe(data => {
     
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

  }

  //Network connectivity 
  displayNetworkUpdate(connectionState: string) {
    let networkType = this.network.type;
    this.toast.create({
      message: `You are now ${connectionState} via ${networkType}`,
      duration: 3000
    }).present();
  }

  // Add Value to Array
  submitButton() {
    //Condtion to check is fields not Null
    if ((this.nameField == undefined && this.nameField == null) || this.nameField == "") {
      this.callalert("Please enter Name");
      return;
    }
    if ((this.phoneField == undefined && this.phoneField == null) || this.phoneField == "") {
      this.callalert("Please enter Phone");
      return;
      // this.phoneField = null
    }
    if(!this.phoneField.match(/^-?(0|[1-9]\d*)?$/)){
      this.callalert("Only Numbers allowed in Phone");
      return;
    }
    if (this.phoneField.length != 10) {
      this.callalert("Phone should be 10 digits");
      return;
    }
    if ((this.gender == undefined && this.gender == null) || this.gender == ""){
      this.callalert("Please select the gender");
      return;
    }

      this.category = {
        "name": this.nameField,
        "mobile": this.phoneField,
        "gender":this.gender,
        "password":"123456",
	      "email":"User1"
      }
      let posturl : string = constant.baseUrl + constant.register
      
    //submit Functionality 
    let loader = this.loading.create();
    loader.present().then(() => {
      if (this.index != null ) {
        this.dataArray[this.index] = this.category;
        this.storage.set("user",this.dataArray);
        this.index = null;
        this.editIndex = null
        this.callalert("User record updated");
        // let alert = this.alertCtrl.create({
        //   message: 'User updated successfully',
        //   buttons: [
        //     {
        //       text: 'ok',
        //       handler: () => {
        //         this.navCtrl.pop();
        //       }
        //     }
        //   ]
        // });
        // alert.present();
      } else {
        this.userProv.postfeedbackData(this.category,posturl).subscribe((data) => {
          if (data['success']) {
            this.dataArray.push(this.category);
            console.log(this.dataArray)
            this.storage.set("user",this.dataArray);
            this.callalert("User created successfully")
          } else {
            this.callalert(data);
            
          }
        })
      }
      
      loader.dismiss();
      this.nameField = "";
      this.phoneField = "";
      this.gender = "Male"


    });
    

    
  }

  // Alert message method
  callalert(message) {
    let alert = this.alertCtrl.create({
      message: message,
      buttons: ['OK']
    })
    alert.present();
    return;
  }

  // Edit Single row data
  editData(i) {

    this.editValue = this.dataArray[i];
    this.phoneField = this.editValue.mobile;
    this.nameField = this.editValue.name;
    this.gender = this.editValue.gender;
    this.index = i;
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController, ViewController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Network } from '@ionic-native/network';
import { HttpRestClientProvider } from '../../providers/http-rest-client/http-rest-client';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { constant } from '../../constant';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  category: any;
  urlLogo = ""
  nameField: string = "User1";
  passField: string = "123456";
  titleHeader: string = "Login";

  constructor(private toast: ToastController, public navCtrl: NavController, private network: Network, public navParams: NavParams, public alertCtrl: AlertController ,
    public _httpRequestProvider: HttpRestClientProvider,
    public loading: LoadingController,public viewCtrl: ViewController,public userProv: UserServiceProvider) {
  }

  ionViewDidLoad() {
  }
  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
}
  //Network Connection 
  ionViewDidEnter() {
    this.network.onConnect().subscribe(data => {
      
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

    this.network.onDisconnect().subscribe(data => {
      
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
  }
  //Network Connection 
  displayNetworkUpdate(connectionState: string) {
    let networkType = this.network.type;
    this.toast.create({
      message: `You are now ${connectionState} via ${networkType}`,
      duration: 3000
    }).present();
  }

  //Login Button with validating fields (User Name :- User1 and password :- 123456)
  submitButton() {
    if ((this.nameField == undefined && this.nameField == null) || this.nameField == "") {
      this.callalert("Please enter User Id");
      return;
    }
    if ((this.passField == undefined && this.passField == null) || this.passField == "") {
      this.callalert("Please enter Password");
      return;
    }
    if ((this.nameField == "User1" ) && this.passField == "123456") {
      let loader = this.loading.create();
      loader.present().then(() => {
      let posturl : string = constant.baseUrl + constant.login
      this.category = {
        "email": this.nameField,
        "password": this.passField
      }
      this.userProv.postfeedbackData(this.category, posturl).subscribe((data) => {
        if (data['token']) {
          
          this.navCtrl.push(HomePage);
        } else {
          this.callalert("Invalid Details")

        }
        
      })
      loader.dismiss();
    });
      
    } else {
      this.callalert("Invalid Details");
      return;
    }
  }

  // Alert Message
  callalert(message) {
    let alert = this.alertCtrl.create({
      message: message,
      buttons: ['OK']
    })
    alert.present();
    return;
  }


}

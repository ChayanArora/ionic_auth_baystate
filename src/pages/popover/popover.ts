import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, LoadingController, App } from 'ionic-angular';
import { constant } from '../../constant';
import { HttpRestClientProvider } from '../../providers/http-rest-client/http-rest-client';
import { NewsfeedPage } from '../newsfeed/newsfeed';
import { ViewUserPage } from '../view-user/view-user';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
// import { LoginPage } from '../login/login';

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html'
})
export class PopoverPage {
  feedData: any;
  page:string;
  constructor(public storage:Storage,public viewCtrl: ViewController, public navCtrl: NavController, public loading: LoadingController, public _httpRequestProvider: HttpRestClientProvider, public appCtrl: App) {
  }
  close() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    this.storage.get("page").then(val=>{
      this.page = val;
      console.log(this.page);
    })
    
  }
  //Log Out method
  logOut() {
    this.viewCtrl.dismiss().then(() => {
      console.log("Logout")
      this.navCtrl.setRoot(LoginPage);
      this.navCtrl.push(LoginPage);
    // this.navCtrl.popToRoot()
    })

    }

  //Go to News Feed Data
  goToNewsFeed() {
    let loader = this.loading.create();
    loader.present().then(() => {
      this._httpRequestProvider.getData(constant.newsfeedUrl)
        .subscribe(res => {
          this.feedData = res;
          this.viewCtrl.dismiss().then(() => {
          this.navCtrl.push(NewsfeedPage, { feedData: this.feedData.articles })
          })
          loader.dismiss();
        },
          error => {

            loader.dismiss();
          })
    })
  }
  goToHomePage() {
    let loader = this.loading.create();
    loader.present().then(() => {
      this.viewCtrl.dismiss().then(() => {
        this.navCtrl.push(HomePage)
      });
      
      loader.dismiss();
    })
  }
  goToViewUser() {
    let loader = this.loading.create();
    loader.present().then(() => {
      this.viewCtrl.dismiss().then(() => {
      this.navCtrl.push(ViewUserPage)
      })
      loader.dismiss();
    })
  }

}

